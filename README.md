# Lucy AI

Client for my AI assistant as extension for [Raycast](https://developers.raycast.com/) app. It is meant to enhance
productivity and automatize every day tasks.

## Developers installation

 - login to Raycast account
 - import extension using `Manage Extension` command in Racast
 - run command `npm install && npm run` to build and deploy locally
 - run command `npm run dev` to start development mode