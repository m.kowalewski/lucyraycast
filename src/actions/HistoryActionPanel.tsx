import { Action, ActionPanel, Icon } from "@raycast/api";
import React from "react";

interface Props {
  openChat: () => void;
}

export default function HistoryActionPanel({ openChat }: Props) {
  return (
    <ActionPanel>
      <Action
        title="Open Conversation"
        icon={Icon.ArrowRightCircleFilled}
        onAction={openChat}
      />
    </ActionPanel>
  );
}