import { Action, ActionPanel, Clipboard, Icon, showHUD } from "@raycast/api";

interface Props {
  answer?: string;
  question: string;
  startNewConversation: () => void;
  toggleFullTextInput: () => void;
  reloadConversation: () => void;
}

export default function ChatActionPanel({
  answer,
  question,
  startNewConversation,
  toggleFullTextInput,
  reloadConversation,
}: Props) {
  return (
    <ActionPanel>
      <Action
        title={"Copy Answer"}
        icon={Icon.CopyClipboard}
        onAction={() => Clipboard.copy(answer ? answer : "").then(() => showHUD("Answer copied!"))}
      />
      <Action
        title={"Copy Question"}
        shortcut={{ modifiers: ["opt"], key: "enter" }}
        icon={Icon.CopyClipboard}
        onAction={() => Clipboard.copy(question).then(() => showHUD("Question copied!"))}
      />
      <Action
        title={"Full Text Input"}
        shortcut={{ modifiers: ["cmd"], key: "t" }}
        icon={Icon.Text}
        onAction={toggleFullTextInput}
      />
      <Action
        title={"Start New Chat"}
        icon={Icon.Repeat}
        shortcut={{ modifiers: ["cmd", "shift"], key: "n" }}
        onAction={startNewConversation}
      />
      <Action
        title={"Reload Conversation"}
        icon={Icon.Repeat}
        shortcut={{ modifiers: ["cmd", "shift"], key: "r" }}
        onAction={reloadConversation}
      />
    </ActionPanel>
  );
}
