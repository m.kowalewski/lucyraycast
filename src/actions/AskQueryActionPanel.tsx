import { Action, ActionPanel, Icon } from "@raycast/api";

interface Props {
  onExecute: () => void;
  startNewConversation: () => void;
  toggleFullTextInput: () => void;
}

export default function AskQueryActionPanel({ onExecute, startNewConversation, toggleFullTextInput }: Props) {
  return (
    <ActionPanel>
      <Action title={"Get Answer"} icon={Icon.CopyClipboard} onAction={onExecute} />
      <Action
        title={"Full Text Input"}
        shortcut={{ modifiers: ["cmd"], key: "t" }}
        icon={Icon.Text}
        onAction={toggleFullTextInput}
      />
      <Action
        title={"Start New Chat"}
        icon={Icon.Repeat}
        shortcut={{ modifiers: ["cmd", "shift"], key: "n" }}
        onAction={startNewConversation}
      />
    </ActionPanel>
  );
}
