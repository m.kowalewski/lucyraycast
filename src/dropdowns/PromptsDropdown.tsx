import { List } from "@raycast/api";
import { MainPrompt, PromptLibrary, PromptsLibraryEnum } from "../models/PromptsLibraryEnum";

interface Props {
  changePromptType: (type: PromptsLibraryEnum) => void;
}

export default function PromptsDropdown({ changePromptType }: Props) {
  const onPromptChange = (typeName: string) => {
    const type = PromptsLibraryEnum[typeName as keyof typeof PromptsLibraryEnum];
    changePromptType(type);
  };

  return (
    <List.Dropdown tooltip="Select Prompt" onChange={(type) => onPromptChange(type)}>
      <List.Dropdown.Section title="Main">
        {MainPrompt.map((prompt) => {
          return <List.Dropdown.Item key={prompt.key} title={prompt.label} value={PromptsLibraryEnum[prompt.key]} />;
        })}
      </List.Dropdown.Section>
      <List.Dropdown.Section title="Prompt library">
        {PromptLibrary.map((prompt) => {
          return <List.Dropdown.Item key={prompt.key} title={prompt.label} value={PromptsLibraryEnum[prompt.key]} />;
        })}
      </List.Dropdown.Section>
    </List.Dropdown>
  );
}
