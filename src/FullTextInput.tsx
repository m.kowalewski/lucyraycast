import { Action, ActionPanel, Form, useNavigation } from "@raycast/api";
import React, { useState } from "react";
import { AskQuestionDto } from "./models/AskQuestionDto";
import { MainPrompt, PromptLibrary, PromptsLibraryEnum } from "./models/PromptsLibraryEnum";

interface Props {
  askQuestion: (dto: AskQuestionDto) => void;
  isiInitialQuestion: boolean;
  defaultValue?: {
    question?: string;
    promptType?: PromptsLibraryEnum;
  };
}

export default function FullTextInputView({ askQuestion, isiInitialQuestion, defaultValue }: Props) {
  const { pop } = useNavigation();

  const [question, setQuestion] = useState<string>(defaultValue?.question || "");
  const [promptType, setPromptType] = useState<PromptsLibraryEnum | undefined>(defaultValue?.promptType);

  const updatePromptType = (typeName: string) => {
    const type = PromptsLibraryEnum[typeName as keyof typeof PromptsLibraryEnum];
    setPromptType(type);
  };

  return (
    <Form
      actions={
        <ActionPanel>
          <Action.SubmitForm
            onSubmit={(values: AskQuestionDto) => {
              askQuestion(values);
              pop();
            }}
          />
        </ActionPanel>
      }
    >
      <Form.TextArea id="question" title="Question" value={question} onChange={setQuestion} />
      {isiInitialQuestion && (
        <Form.Dropdown id="promptType" title="Prompt type" value={promptType} onChange={updatePromptType}>
          <Form.Dropdown.Section title="Main">
            {MainPrompt.map((prompt) => {
              return (
                <Form.Dropdown.Item key={prompt.key} title={prompt.label} value={PromptsLibraryEnum[prompt.key]} />
              );
            })}
          </Form.Dropdown.Section>
          <Form.Dropdown.Section title="PromptLibrary">
            {PromptLibrary.map((prompt) => {
              return (
                <Form.Dropdown.Item key={prompt.key} title={prompt.label} value={PromptsLibraryEnum[prompt.key]} />
              );
            })}
          </Form.Dropdown.Section>
        </Form.Dropdown>
      )}
    </Form>
  );
}
