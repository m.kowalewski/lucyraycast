import { PromptsLibraryEnum } from "./PromptsLibraryEnum";

export interface AskQuestionDto {
  question: string;
  promptType?: PromptsLibraryEnum;
}
