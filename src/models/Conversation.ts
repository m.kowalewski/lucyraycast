export interface ConversationListDto {
  id: string;
  summary: string;
  createdAt: Date;
}

export interface ConversationDto {
  id?: string;
  messages: MessageDto[];
}

export interface MessageDto {
  id?: string;
  conversationId?: string;
  role: ConversationRole;
  message: string;
  sentTime?: string;
}

export enum ConversationRole {
  SYSTEM = "SYSTEM",
  USER = "USER",
  ASSISTANT = "ASSISTANT",
}
