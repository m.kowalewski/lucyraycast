export enum PromptsLibraryEnum {
  MAIN_LUCY_PROMPT = "MAIN_LUCY_PROMPT",
  BUG_FIXER = "BUG_FIXER",
  SOLUTION_ARCHITECT = "SOLUTION_ARCHITECT",
  LINKEDIN_GHOSTWRITER = "LINKEDIN_GHOSTWRITER",
  PROMPT_ENGINEER = "PROMPT_ENGINEER",
  BASIC_RAG = "BASIC_RAG",
  DOCUMENTATION_WRITER = "DOCUMENTATION_WRITER",
}

export const MainPrompt = [
  {
    key: PromptsLibraryEnum.MAIN_LUCY_PROMPT,
    label: "Lucy",
  },
];

export const PromptLibrary = [
  {
    key: PromptsLibraryEnum.BUG_FIXER,
    label: "Bug fixer",
  },
  {
    key: PromptsLibraryEnum.SOLUTION_ARCHITECT,
    label: "Solution Architect",
  },
  {
    key: PromptsLibraryEnum.LINKEDIN_GHOSTWRITER,
    label: "Linkedin Ghostwriter",
  },
  {
    key: PromptsLibraryEnum.DOCUMENTATION_WRITER,
    label: "Documentation Writer",
  },
  {
    key: PromptsLibraryEnum.PROMPT_ENGINEER,
    label: "Prompt Engineer",
  },
  {
    key: PromptsLibraryEnum.BASIC_RAG,
    label: "Basic RAG",
  },
];
