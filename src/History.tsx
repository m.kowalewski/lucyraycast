import { List, useNavigation } from "@raycast/api";
import HistoryActionPanel from "./actions/HistoryActionPanel";
import ChatView from "./Chat";
import { useConversations } from "./hooks/useConversations";
import { useEffect, useState } from "react";
import { ConversationListDto } from "./models/Conversation";

export default function HistoryView() {

  const conversations = useConversations();

  const [history, setHistory] = useState<ConversationListDto[]>([]);
  const [isLoading, setLoading] = useState<boolean>(true);

  const { push } = useNavigation();

  useEffect(() => {
    conversations.fetchHistory()
      .then((response) => {
        setHistory(response ?? []);
        setLoading(false);
      });
  }, []);

  return (
    <List
      isShowingDetail={false}
      isLoading={isLoading}
    >
      {history
        .map((conversation) => {
          return (
            <List.Item
              key={conversation.id}
              title={conversation.summary}
              actions={
                <HistoryActionPanel
                  openChat={() => push(<ChatView conversationId={conversation.id} />)}
                />
              }
            />
          );
        })
      }
    </List>
  );

}
