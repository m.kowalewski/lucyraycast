import { clearSearchBar, List, useNavigation } from "@raycast/api";
import { useEffect, useState } from "react";
import ChatActionPanel from "./actions/ChatActionPanel";
import AskQueryActionPanel from "./actions/AskQueryActionPanel";
import { ConversationDto, ConversationRole, MessageDto } from "./models/Conversation";
import { useConversations } from "./hooks/useConversations";
import { randomUUID } from "node:crypto";
import FullTextInputView from "./FullTextInput";
import { AskQuestionDto } from "./models/AskQuestionDto";
import PromptsDropdown from "./dropdowns/PromptsDropdown";
import { PromptsLibraryEnum } from "./models/PromptsLibraryEnum";
import { runAppleScript } from "@raycast/utils";

export default function ChatView(props: { conversationId?: string }) {
  const { push } = useNavigation();
  const conversationsMemory = useConversations();

  const [question, setQuestion] = useState("");
  const [selectedPromptType, setPromptType] = useState<PromptsLibraryEnum | undefined>(undefined);
  const [conversation, setConversation] = useState<ConversationDto>({ messages: [] });
  const [questionsCount, setQuestionsCount] = useState<number>(0);
  const [selectedMessageId, setSelectedMessageId] = useState<string | null>(null);
  const [preventAutomaticallyChangeSelection, setPreventAutomaticallyChangeSelection] = useState<boolean>(false);
  const [isLoading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    loadConversation();
  }, []);

  function loadConversation() {
    // TODO: mkowalewski - load conversation hook
    const conversationId = conversation.id || props.conversationId;

    if (!conversationId) {
      setConversation({ messages: [] });
      setLoading(false);
      return;
    }

    conversationsMemory.fetchConversation(conversationId).then((response) => {
      const conversation = response ?? { messages: [] };
      conversation.messages.sort((a, b) => {
        const timeA = a.sentTime ? new Date(a.sentTime).getTime() : Number.MIN_SAFE_INTEGER;
        const timeB = b.sentTime ? new Date(b.sentTime).getTime() : Number.MIN_SAFE_INTEGER;
        return timeB - timeA;
      });
      setConversation(conversation);
      setPromptType(undefined);
      const count = conversation.messages.filter((mes) => mes.role === ConversationRole.USER).length;
      setQuestionsCount(count);
      setLoading(false);
    });
  }

  const askQuestion = (dto: AskQuestionDto) => {
    setLoading(true);
    setPromptType(dto.promptType);

    const questionDto: MessageDto = {
      id: randomUUID(), // for list purpose, do not send on backend
      role: ConversationRole.USER,
      message: dto.question,
    };

    setConversation((currentConversation) => ({
      ...currentConversation,
      messages: [questionDto, ...currentConversation.messages],
    }));
    // We need to set locking flag because when we don't it will fall in infinite loop of changes
    setPreventAutomaticallyChangeSelection(true);
    setSelectedMessageId(questionDto.id || null);

    setQuestionsCount(questionsCount + 1);

    clearSearchBar().then(() => setQuestion(""));

    conversationsMemory
      .sendMessage({
        conversationId: conversation.id,
        promptType: dto.promptType || selectedPromptType,
        messages: [questionDto],
      })
      .then((response) => {
        setLoading(false);
        setPromptType(undefined);
        setConversation((prevState) => {
          const newState = { ...prevState };
          newState.id = response.conversationId;
          newState.messages = [response, ...newState.messages];
          return newState;
        });
        runAppleScript(`display notification "Lucy just respond you!" with title "Lucy" sound name "Glass"`, []);
      });
  };

  const toggleFullTextInput = () => {
    clearSearchBar();
    push(
      <FullTextInputView
        askQuestion={(dto: AskQuestionDto) => askQuestion(dto)}
        isiInitialQuestion={!conversation.id}
        defaultValue={{ question: question, promptType: selectedPromptType }}
      />,
    );
  };

  const startNewConversation = () => {
    clearSearchBar().then(() => setQuestion(""));
    setConversation({ messages: [] });
    setQuestionsCount(0);
    setLoading(false);
  };

  const findResponseTo = (question: MessageDto) => {
    let questionIndex = conversation.messages.findIndex((el) => el.id === question.id);
    while (
      conversation.messages[questionIndex] &&
      conversation.messages[questionIndex].role !== ConversationRole.ASSISTANT
    ) {
      questionIndex--;
    }
    return conversation.messages[questionIndex];
  };

  return (
    <List
      isShowingDetail={true}
      filtering={false}
      searchBarPlaceholder={conversation?.messages.length > 0 ? "Ask next question..." : "Ask a question..."}
      onSearchTextChange={(text) => setQuestion(text)}
      searchBarAccessory={
        conversation.messages.length > 0 ? undefined : (
          <PromptsDropdown changePromptType={(type) => setPromptType(type)} />
        )
      }
      actions={
        conversation?.messages.length > 0 ? undefined : (
          // TODO: mkowalewski - hide "Get Answer" option if there is no question
          <AskQueryActionPanel
            onExecute={() => askQuestion({ question: question })}
            startNewConversation={startNewConversation}
            toggleFullTextInput={toggleFullTextInput}
          />
        )
      }
      isLoading={isLoading}
      selectedItemId={selectedMessageId || undefined}
      onSelectionChange={(id) => {
        if (preventAutomaticallyChangeSelection) {
          setPreventAutomaticallyChangeSelection(false);
        } else if (id !== selectedMessageId) {
          setSelectedMessageId(id);
        }
      }}
    >
      {conversation.messages
        .filter((mes) => mes.role === ConversationRole.USER)
        .map((message, index) => {
          const props: Partial<List.Item.Props> = {
            detail: (
              <List.Item.Detail markdown={`${message.message}\n\n---\n\n${findResponseTo(message)?.message || ""}`} />
            ),
          };

          return (
            <List.Item
              id={message.id}
              key={message.id}
              title={message.message}
              subtitle={`#${questionsCount - index}`}
              {...props}
              actions={
                question ? (
                  <AskQueryActionPanel
                    onExecute={() => askQuestion({ question: question })}
                    startNewConversation={startNewConversation}
                    toggleFullTextInput={toggleFullTextInput}
                  />
                ) : (
                  <ChatActionPanel
                    answer={findResponseTo(message)?.message}
                    question={message.message}
                    startNewConversation={startNewConversation}
                    toggleFullTextInput={toggleFullTextInput}
                    reloadConversation={loadConversation}
                  />
                )
              }
            />
          );
        })}
    </List>
  );
}
