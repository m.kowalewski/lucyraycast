import axios from "axios";
import { getPreferenceValues, showToast, Toast } from "@raycast/api";
import { ConversationDto, ConversationListDto, MessageDto } from "../models/Conversation";

interface Preferences {
  apiUrl: string;
  apiKey: string;
  useLocalConfig: boolean;
  localApiUrl: string;
  localApiKey: string;
}

export const useConversations = () => {
  const PREFERENCES = getPreferenceValues<Preferences>();
  const API_URL = PREFERENCES.useLocalConfig ? PREFERENCES.localApiUrl : PREFERENCES.apiUrl;
  const GET_CONVERSATION_LIST_URL = API_URL + "/conversation/list";
  const GET_CONVERSATION_URL = API_URL + "/conversation/:conversationId";
  const SEND_MESSAGE_URL = API_URL + "/assistant/send-message/synchronously";
  const HEADERS = {
    "X-API-KEY": PREFERENCES.useLocalConfig ? PREFERENCES.localApiKey : PREFERENCES.apiKey,
  };

  const fetchHistory = async () => {
    const toast = await showToast({
      style: Toast.Style.Animated,
      title: "Fetching history... ⏳,",
    });
    try {
      const response = await axios.get<ConversationListDto[]>(GET_CONVERSATION_LIST_URL, { headers: HEADERS });

      toast.style = Toast.Style.Success;
      toast.title = "Got it! 😀";

      return response.data;
    } catch (error: unknown) {
      handleApiException(error, toast);
    }
  };

  const fetchConversation = async (conversationId: string) => {
    const toast = await showToast({
      style: Toast.Style.Animated,
      title: "Fetching conversation... ⏳",
    });

    try {
      const url = GET_CONVERSATION_URL.replace(":conversationId", conversationId);
      const response = await axios.get<ConversationDto>(url, { headers: HEADERS });

      toast.style = Toast.Style.Success;
      toast.title = "Got it! 😀";

      return response.data;
    } catch (error: unknown) {
      handleApiException(error, toast);
      throw error;
    }
  };

  const sendMessage = async (data: object) => {
    const toast = await showToast({
      style: Toast.Style.Animated,
      title: "Answering your question... 🧐",
    });

    try {
      const response = await axios.post<MessageDto>(SEND_MESSAGE_URL, data, { headers: HEADERS });

      toast.style = Toast.Style.Success;
      toast.title = "Got it! 😀";
      return response.data;
    } catch (error: unknown) {
      handleApiException(error, toast);
      throw error;
    }
  };

  const handleApiException = (error: unknown, toast: Toast) => {
    toast.style = Toast.Style.Failure;
    if (axios.isAxiosError(error)) {
      toast.title = error.message;
    } else {
      toast.title = "Unexpected error.";
    }
  };

  return { fetchHistory, fetchConversation, sendMessage };
};
