import { Detail } from "@raycast/api";
import React, { useEffect, useState } from "react";
import fetch from "node-fetch";

// to add paste this json to package json
// {
//    "name": "test",
//    "title": "Test",
//    "description": "Test some new techniques",
//    "mode": "view"
// }

export default function Test() {
  const [question, setQuestion] = useState(
    "**Hello test**! \n\n _test 123_ \n\n ![](https://theduck.bieda.it/lucy/v1/assistant/image/ea1bfe8d-de2b-4544-a405-6f4240160b9f)",
  );
  const apiKey = "";

  useEffect(() => {
    fetchAndReplaceImageUrls(question, apiKey).then((value) => setQuestion(value));
  });

  return <Detail markdown={question}></Detail>;
}

const arrayBufferToBase64 = (buffer: ArrayBuffer): string => {
  let binary = "";
  const bytes = new Uint8Array(buffer);
  const len = bytes.byteLength;
  for (let i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return Buffer.from(binary, "binary").toString("base64");
};

const fetchImageAsBase64 = async (url: string, apiKey: string): Promise<string> => {
  const response = await fetch(url, {
    headers: {
      "X-API-KEY": apiKey,
    },
  });
  const arrayBuffer = await response.arrayBuffer();
  const base64String = arrayBufferToBase64(arrayBuffer);
  const contentType = response.headers.get("Content-Type") || "image/png";
  return `data:${contentType};base64,${base64String}`;
};

const fetchAndReplaceImageUrls = async (markdown: string, apiKey: string): Promise<string> => {
  let updatedMarkdown = markdown;

  const urlRegex = /!\[\]\((https:\/\/theduck\.bieda\.it\/lucy\/v1\/assistant\/image\/[0-9a-fA-F-]+)\)/g;

  // Extract URLs and replace them with Base64 data URLs
  const matches = [...markdown.matchAll(urlRegex)];
  for (const match of matches) {
    const url = match[1];
    try {
      const dataUrl = await fetchImageAsBase64(url, apiKey);
      updatedMarkdown = updatedMarkdown.replace(url, dataUrl);
    } catch (error) {
      console.error(`Error fetching image from ${url}:`, error);
    }
  }

  return updatedMarkdown;
};
